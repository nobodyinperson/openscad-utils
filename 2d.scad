////////////////////////
/// ⬜ 2D operations ///
////////////////////////

module
offset_chain(offsets, chamfer = false)
{
  module offset_(x)
  {
    if (chamfer)
      offset(delta = x, chamfer = true) children();
    else
      offset(r = x) children();
  }
  if (len(offsets) <= 0)
    children();
  else if (len(offsets) == 1)
    offset_(offsets[0]) children();
  else
    offset_chain([for (i = [1:len(offsets) - 1]) offsets[i]], chamfer = chamfer)
      offset_(offsets[0]) children();
}

////////////////////
/// ⬜ 2D shapes ///
////////////////////

// points for use in polygon() to create a part of a cylinder
function wedge_points(angle = 360,
                      radius = 1,
                      $fn = undef,
                      $fa = $fa,
                      $fs = $fs) =
  let(n = (is_undef($fn) ? max(angle / $fa, 2 * PI / 360 * angle * radius / $fs)
                         : $fn))
    concat([[ 0, 0 ]],
           [for (i = [0:n])
               multiply([ cos(angle * i / n), sin(angle* i / n) ], radius)],
           [multiply([ cos(angle), sin(angle) ], radius)]);

module
wedge(angle = 360,
      radius = 1,
      height = 1,
      center = false,
      $fn = undef,
      $fa = $fa,
      $fs = $fs)
{
  linear_extrude(height, center = center) polygon(wedge_points(
    angle = angle, radius = radius, $fn = $fn, $fa = $fa, $fs = $fs));
}

// round edges of 2d shape
module
edgeround(radius, outwards = false)
{
  if (outwards)
    offset(-radius) offset(radius) children();
  else
    offset(radius) offset(-radius) children();
}

// „hole out” a 2D shape to use less material
module
holeout2dshape(xymax,
               grid_hole_size = 1,
               grid_edge_radius = 0,
               grid_spoke_width = 1,
               grid_angle = 0,
               grid_offset = [ 0, 0 ], )
{
  assert($children >= 1, "A child is needed!");
  edgeround(min([ grid_edge_radius, grid_hole_size / 2 * 0.99 ]),
            outwards = true)
  {
    difference()
    {
      children();
      offset(-grid_spoke_width) children();
    }
    intersection()
    {
      children();
      rotate([ 0, 0, grid_angle ]) difference()
      {
        circle(r = xymax);
        n = ceil(xymax / (grid_hole_size + grid_spoke_width));
        edgeround(min([
                    // grid_spoke_width / 2 * 0.99,
                    grid_hole_size / 2 * 0.99,
                    grid_edge_radius
                  ]),
                  outwards = false)
          mosaic([-n:n], [-n:n], distance = grid_hole_size + grid_spoke_width)
            square([ grid_hole_size, grid_hole_size ], center = true);
      }
    }
  }
  if (grid_edge_radius > 0)
    difference()
    {
      children();
      offset(-grid_spoke_width * 0.99) children();
    }
}

module
chained_polygon_hull(
  generator = function(x, length)[[ 1, 1 ], [ -1, 1 ], [ -1, -1 ], [ 1, -1 ]],
  length = 1,
  slices = undef,
  $fn = $fn,
  $fa = $fa,
  $fs = $fs)
{
  n_max = max([ $fn, ceil(360 / $fa) ]);
  hdiff = length / n_max;
  n_segments = ceil(length / hdiff);
  epsilon = hdiff * 0.1;

  module segment(x)
  {
    points = generator(x = x, length = length);
    translate([ 0, 0, x * length + (2 * x * epsilon - epsilon) ])
      linear_extrude(epsilon / 100) polygon(points);
  }
  for (i = [0:n_segments - 1]) {
    h = i * hdiff;
    hull()
    {
      segment(h / length);
      segment((h + hdiff) / length);
    }
  }
}
