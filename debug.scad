//////////////////////////
/// 🔎 Debug utilities ///
//////////////////////////

module
debug(when = true)
{
  if (when) {
    % children();
  } else {
    children();
  }
}

// for backwards-compatibility, use debug() instead
module
debug_if(x)
{
  debug(when = x) children();
}

module
highlight(when = true)
{
  if (when) {
#children();
  } else {
    children();
  }
}

// for backwards-compatibility, use highlight() instead
module
highlight_if(x)
{
  highlight(when = x) children();
}

module
show_exclusively(when = true)
{
  if (when) {
    !children();
  } else {
    children();
  }
}

module
show_exclusively_if(x)
{
  show_exclusively_if(when = x) children();
}

module
render_if(x, convexity = undef)
{
  if (x)
    render(convexity = convexity) children();
  else
    children();
}

module
color_if(x, c = undef, alpha = 1)
{
  if (x)
    color(c, alpha) children();
  else
    children();
}

module
color_(c, alpha = 1, when = true)
{
  if (when)
    color(c, alpha) children();
  else
    children();
}

module
resize_if(x, size, auto = false)
{
  if (x)
    resize(size, auto = auto) children();
  else
    children();
}

module
hull_(when = true)
{
  if (when)
    hull() children();
  else
    children();
}
