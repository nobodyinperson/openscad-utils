// Move/Copy all children to all given positions
//
// ”positions” is a list of positions to move to. Each position can be a list
// itself which will be interpreted as an [x,y,z]-tuple (possibly filling
// missing numbers with 0 to the right) or a single number, which is then
// interpreted as the coordinate given as the ”default” argument (e.g.
// at([1,2,3],"z"){...} will move/copy all children by z coordinate 1, 2 and 3)
module
at(positions, default = "x")
{
  default_ind = search(default, "xyz")[0];
  for (i = [0:max(0, ($children - 1))]) {
    for (position = positions) {
      // make sure the input is correct
      assert((is_list(position) && (0 < len(position) && len(position) <= 3)) ||
               is_num(position),
             "only numeric/3-vector positions are allowed");
      pos = is_list(position)
              // fill with zeros to the right
              ? [for (j = [0:2]) is_num(position[j]) ? position[j] : 0]
              // for single numbers use that for the ”default” dimension
              : [for (j = [0:2]) j == default_ind ? position : 0];
      // move all children to all positions
      translate(pos) children(i);
    }
  }
}

// arranges children in a mosaic pattern
module mosaic(
  // sequence of elements in x direction
  ix = [0],
  // sequence of elements in y direction
  iy = [0],
  // distance between elements
  distance = 1,
  // positioning function
  position = function(i, j, d)[i * d, j* d],
  // offset function
  offset = function(i, j, d)[0, 0],
  // rotation function
  rotation = function(i, j, d)[0, 0],
  // function to decide whether to draw an element
  decider = function(i, j, d) true,
  // function to set the color of the element
  color = function(i, j, d) undef)
{
  for (i = ix) {
    for (j = iy) {
      if (decider(i, j, distance)) {
        translate(position(i, j, distance)) translate(offset(i, j, distance))
          rotate(rotation(i, j, distance))
        {
          c = color(i, j, distance);
          color_if(!is_undef(c),
                   is_list(c) ? c[0] : c,
                   alpha = is_list(c) ? c[1] : 1) children();
        }
      }
    }
  }
}

// Off-the-shelf chained hull (only hull()ing consequtive children)
module
chained_hull()
{
  if ($children <= 1) {
    children();
  } else {
    for (i = [0:$children - 2]) {
      hull()
      {
        children(i);
        children(i + 1);
      }
    }
  }
}

module
local_xyz(length = 115,
          thickness = 1,
          ticks = [ 100, 10, 1 ],
          tip_diameter = undef,
          tip_length = undef,
          letter_size = undef,
          show = true)
{
  td = is_undef(tip_diameter) ? thickness * 3 : tip_diameter;
  tl = is_undef(tip_length) ? td * 3 : tip_length;
  ls = is_undef(letter_size) ? td * 3 : letter_size;
  module axis(letter)
  {
    rotate([ 0, 90, 0 ])
    {
      // the axis
      translate([ 0, 0, length / 2 ])
        cylinder(d = thickness, h = length, center = true, $fn = 4);
      // tip
      translate([ 0, 0, length ]) cylinder(d1 = td, d2 = 0, h = 5, $fn = 4);
      // ticks
      for (tickdiff = ticks) {
        nticks = floor(length / tickdiff);
        for (itick = [1:nticks]) {
          translate([ 0, 0, itick * tickdiff ])
            cylinder(d = thickness + tickdiff / max(ticks) * 3 * thickness,
                     h = min([ $fs, 0.1 ]),
                     center = true,
                     $fn = 4);
        }
      }
    }
    // letter
    translate([ length + tl * 2, 0 ]) rotate([ 0, 0, -90 ])
      linear_extrude(thickness, center = true)
        text(letter, size = ls, halign = "center", valign = "center");
  }
  if (show && $preview) {
    // make all axes
    color("red") rotate([ 0, 0, 0 ]) axis("x");
    color("lightgreen") rotate([ 0, 0, 90 ]) axis("y");
    color("blue") rotate([ 0, -90, 0 ]) axis("z");
  }
  // show all children as well
  children();
}

module
translate_(pos, when = true)
{
  if (when)
    translate(pos) children();
  else
    children();
}
