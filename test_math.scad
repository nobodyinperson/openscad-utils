// math tests

// clang-format off
include <math.scad>;
// clang-format on

// test round_to()
assert(round_to(31.5, 5) == 30);
assert(round_to(1.2, 0.25) == 1.25);
assert(round_to(-750, 360) == -720);

// test normalize_angle()
for (angle = [1:1:359]) {
  for (multiple = [1:4]) {
    // echo(angle = angle, multiple = multiple);
    // echo(normalize_angle_multiple = normalize_angle(angle + 360 * multiple));
    assert(normalize_angle(angle + 360 * multiple) == angle);
    // echo(normalize_neg_angle_multiple =
    //        normalize_angle(-angle - 360 * multiple));
    // echo(normalize_neg_angle_multiple_right = -angle + 360);
    assert(normalize_angle(-angle + 360 * multiple) == 360 - angle);
  }
}

// test angle_between()
assert(angle_between(10, 0, 90) == true);
assert(angle_between(10, 90, 0) == false);
assert(angle_between(208, 80, 320) == true);
assert(angle_between(208, 320, 80) == false);
assert(angle_between(103, 314, 28) == false);
assert(angle_between(103, 28, 314) == true);
assert(angle_between(0, -28, 1) == true);
assert(angle_between(0, 1, -28) == false);
for (angle = [1:360]) {
  assert(angle_between(angle, angle - 1, angle + 1) == true);
  assert(angle_between(angle, angle + 1, angle - 1) == false);
  assert(angle_between(angle, angle - 10, angle + 10) == true);
  assert(angle_between(angle, angle + 10, angle - 10) == false);
  assert(angle_between(angle, angle - 179.99, angle + 179.99) == true);
  assert(angle_between(angle, angle + 179.99, angle - 179.99) == false);
  assert(angle_between(angle, angle, angle) == true);
}
