// repeat an element
function repeat(element, count) = [for (i = [1:count]) element];

// reverse the elements of a list
function reverse(list) = [for (i = [len(list) - 1:-1:0]) list[i]];

// reduce a list to a scalar value using an operator
// (i.e. take two elements, apply operator, then apply operator to result and
// next element, and so on - e.g. reduce(function(x,y)x+y,[1,2,3,4]) == 10)
function reduce(operator, x, initial = undef) =
  // debug output
  // let(bla = echo("reduce()", operator= operator, x = x, initial = initial))
  is_undef(initial)
    // case without initial=
    ? (len(x) <= 1
         ? x[0]
         : (len(x) == 2 ? operator(x[0], x[1]) :
                        operator(x[0],
                                 reduce(operator,
                                        [for (i = [1:len(x) - 1]) x[i]]))))
    // case with initial=
    : (len(x) < 1 ? initial :
                  operator(initial, reduce(operator, x, initial = undef)));

all = function(x) reduce(and_, x);
any = function(x) reduce(or_, x);

function sum(list) = reduce(add_, list, 0);
function mean(list) = sum(list) / len(list);
function product(list) = reduce(multiply, list);

function map(operator, list) = [for (x = list) operator(x)];

// given a list, return a list with N elements grouped at a time
//
// This is useful for reshaping one-dimensional lists
// (e.g. groupn([1,2,3,4],2) == [[1,2],[3,4]])) which can be a workaround for
// #3817 (Customizer not being able to restore nested vectors - but
// one-dimensional ones!)
function groupn(list, ncol) = let(nrow = ceil(len(list) / ncol))[for (
  row = (nrow > 0 ? [0:nrow - 1]
                  : []))[for (col = [0:ncol - 1]) list[row * ncol + col]]];

// reshape() - (Un)nest list elements to switch between dimensionalities
//
// This is useful for reshaping one-dimensional lists (e.g.
// reshape([1,2,3,4],[2,2]) == [[1,2],[3,4]])) which can be a workaround for
// #3817 (Customizer not being able to restore nested vectors - but
// one-dimensional ones!)
function reshape(list, shape) = reduce(function(n, l) groupn(l, n),
                                       concat(shape, [flatten(list)]))[0];

////////////////////////////////////
/// 🔄 recursive list operations ///
////////////////////////////////////

// flatten an arbitrarily deeply nested list
function flatten(list) = any(map(function(x) is_list(x), list))
                           ? reduce(function(x, y) concat(x, y),
                                    [for (x = list) flatten(x)])
                           : list;

// This function applies an operator between scalars and/or corresponding
// elements of lists
function operate(x, y, operator) =
  is_list(x)
    ? (is_list(y) ? [for (i = [0:(max(len(x), len(y)) - 1)])
                        operate(x[i], y[i], operator)]
                  : [for (x_ = x) operate(x_, y, operator)])
    : (is_list(y) ? [for (y_ = y) operate(x, y_, operator)] : operator(x, y));

// shortcuts of operate() for the common mathematical operators
multiply = function(x, y) operate(x, y, multiply_);
divide = function(x, y) operate(x, y, divide_);
add = function(x, y) operate(x, y, add_);
subtract = function(x, y) operate(x, y, subtract_);
