// clang-format off
include <itertools.scad>;
include <math.scad>;
// clang-format on

// Add rounding functionality to linear_extrude()
//
// rtop: the radius to round the TOP part
// rbottom: the radius to round the BOTTOM part
//
// Warning: This only works well for extruding CONVEX 2D shapes.
module
linear_extrude_rounded_convex(length,
                              rtop = 0,
                              angletop = 90,
                              rbottom = 0,
                              anglebottom = 90,
                              center = false,
                              $fn = $fn,
                              $fs = $fs,
                              $fa = $fa)
{
  translate(center ? [ 0, 0, -length / 2 ] : [ 0, 0, 0 ])
  {
    epsilon = 1e-10;
    if (rbottom != 0) {
      n =
        ceil(min(concat([ anglebottom / $fa, 2 * PI * abs(rbottom) / 4 / $fs ],
                        $fn > 0 ? $fn : [])));
      translate([ 0, 0, 0 ]) for (i = [1:n])
      {
        angle = anglebottom * i / n;
        angleprev = anglebottom * (i - 1) / n;
        shrink = rbottom * (1 - cos(angle));
        shrinkprev = rbottom * (1 - cos(angleprev));
        height = (sin(anglebottom) - sin(angle)) * abs(rbottom);
        heightprev = (sin(anglebottom) - sin(angleprev)) * abs(rbottom);
        hull()
        {
          translate([ 0, 0, heightprev ]) linear_extrude(epsilon)
            offset(-shrinkprev) children();
          translate([ 0, 0, height - epsilon ]) linear_extrude(epsilon)
            offset(-shrink) children();
        }
      }
    }
    hull()
    {
      translate(
        [ 0, 0, abs(rbottom) * sin(anglebottom) + (rbottom > 0 ? epsilon : 0) ])
        linear_extrude(epsilon) children();
      translate(
        [ 0, 0, length - abs(rtop) * sin(angletop) - (rtop > 0 ? epsilon : 0) ])
        linear_extrude(epsilon) children();
    }
    if (rtop != 0)
      translate([ 0, 0, max(0, length - abs(rtop) * sin(angletop)) ])
      {
        n = ceil(min(concat([ angletop / $fa, 2 * PI * rtop / 4 / $fs ],
                            $fn > 0 ? $fn : [])));
        for (i = [1:n]) {
          angle = angletop * i / n;
          angleprev = angletop * (i - 1) / n;
          shrink = rtop * (1 - cos(angle));
          shrinkprev = rtop * (1 - cos(angleprev));
          height = sin(angle) * abs(rtop);
          heightprev = sin(angleprev) * abs(rtop);
          hull()
          {
            translate([ 0, 0, heightprev ]) linear_extrude(epsilon)
              offset(-shrinkprev) children();
            translate([ 0, 0, height - epsilon ]) linear_extrude(epsilon)
              offset(-shrink) children();
          }
        }
      }
  }
}

// Sequence of connected cylinders stacked in z-direction
// The 'cylinders' argument is a list of cylinder definitions, each
// element of which is a list of 3 elements [diameter, height, $fn, rotation]
// (where $fn and 'rotation' can be omitted)
// 'overlap' whether segments of smaller diameter should reach into segments
// of larger diameters (top and bottom surface are handled separately). Half of
// the height of the shortest segment is used as overlap length. This helps
// unioning the segments together and prevents non-manifold rendering
// issues with difference(). Set it to false to disable this.
//
// Examples:
//
// A cylindrical ”wedding cake” with three layers:
//   cylinder_sequence([ [ 30, 5 ], [ 20, 5 ], [ 10, 5 ] ])
//
// A tower with less and less edges the higher it goes:
//   cylinder_sequence([for (n = [10:-1:3])[n, n, n]]);
//
// An épée/sword:
//   cylinder_sequence([ [ 2, 5, 4 ], [ 10, 1 ], [ 1, 50 ] ]);
module
cylinder_sequence(cylinders, overlap = true)
{
  overlap_ =
    overlap
      ? (len(cylinders) > 0 ? min([for (part = cylinders) part[0]]) / 2 : 0)
      : 0;
  for (ipart = len(cylinders) > 0 ? [0:len(cylinders) - 1] : []) {
    part = cylinders[ipart];
    extend_below = ipart > 0 ? cylinders[ipart - 1][0] > part[0] : false;
    extend_above =
      ipart < (len(cylinders) - 1) ? cylinders[ipart + 1][0] > part[0] : false;
    translate([
      0,
      0,
      ipart > 0 ? reduce(add_, [for (i = [0:ipart - 1]) cylinders[i][1]]) : 0
    ]) rotate([ 0, 0, is_num(part[3]) ? part[3] : 0 ])
      translate([ 0, 0, extend_below ? -overlap_ : 0 ])
        cylinder(d = part[0],
                 h = part[1] + (extend_above ? overlap_ : 0) +
                     (extend_below ? overlap_ : 0),
                 $fn = part[2]);
  }
}
