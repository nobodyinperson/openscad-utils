/////////////////////////
/// 🔢 math functions ///
/////////////////////////

// the basic operators as lambdas

multiply_ = function(x, y) x * y;
divide_ = function(x, y) x / y;
add_ = function(x, y) x + y;
subtract_ = function(x, y) x - y;
and_ = function(x, y) x && y;
or_ = function(x, y) x || y;
equals_ = function(x, y) x == y;
differs_ = function(x, y) x != y;

// exponentiation function fixed for exponentiating negative numbers to
// decimal powers
function pow2(x, y) = pow(abs(x), y) * sign(x);

function round_to(x, to = 1) = round(x / to) * to;

function normalize_angle(angle) = (angle +
                                   max([ 360, abs(round_to(angle, 360)) ])) %
                                  360;

// is given angle (counter-clockwise) between lower and upper limit?
function angle_between(angle, lower = 0, upper = 360) =
  let(a = normalize_angle(angle),
      l = normalize_angle(lower),
      u = normalize_angle(upper))(u > l)
    ? ((l <= a) && (a <= u))
    : ((l > u) ? !((u < a) && (a < l)) : ((l == a) && (a == u)));

