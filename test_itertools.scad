// clang-format off
include <itertools.scad>;
include <math.scad>;
// clang-format on

// repeat test
assert(repeat(10, 5) == [ 10, 10, 10, 10, 10 ]);
assert(repeat([ 1, 2, 3 ], 2) == [ [ 1, 2, 3 ], [ 1, 2, 3 ] ]);

// reverse() test
assert(reverse([ 1, 2, [ 3, 4 ] ]) == [ [ 3, 4 ], 2, 1 ]);

// reduce(tests)
assert(reduce(add_, reverse([ 1, 2, 3, 4 ])) == 10);
assert(reduce(multiply_, [ 1, 2, 3, 4 ]) == 24);
assert(reduce(add_, [ 1, 2, 3, 4 ], 5) == 15);

// all() and any() tests
assert(all([ true, true, false ]) == false);
assert(all([ true, true, true ]) == true);
assert(all([ false, false, false ]) == false);
assert(any([ true, true, false ]) == true);
assert(any([ true, true, true ]) == true);
assert(any([ false, false, false ]) == false);

// sum() and product() tests
assert(sum([ 1, 2, 3, 4 ]) == 10);
assert(mean([ 1, 2, 3, 4 ]) == 2.5);
assert(product([ 1, 2, 3, 4 ]) == 24);

// map() tests
assert(map(function(x) 2 * x, [ 1, 2, 3 ]) == [ 2, 4, 6 ]);

// groupn() tests
let(grouped = groupn([ 1, 2, 3, 4 ], 2))
{
  assert(grouped == [ [ 1, 2 ], [ 3, 4 ] ]);
  assert(grouped[0][0] == 1);
  assert(grouped[1][1] == 4);
}
assert(groupn([ 1, 2, 3, 4, 5, 6, 7, 8 ], 4) ==
       [ [ 1, 2, 3, 4 ], [ 5, 6, 7, 8 ] ]);
assert(groupn([], 2) == []);

// reshape() tests
assert(reshape([ 1, 2, 3, 4, 5, 6 ], [ 2, 3 ]) == [ [ 1, 2, 3 ], [ 4, 5, 6 ] ]);
assert(reshape([ 1, 2, 3, 4, 5, 6 ], [ 3, 2 ]) ==
       [ [ 1, 2 ], [ 3, 4 ], [ 5, 6 ] ]);
assert(reshape([ 1, 2, 3, 4, 5, 6, 7, 8 ], [ 2, 2, 2 ]) ==
       [ [ [ 1, 2 ], [ 3, 4 ] ], [ [ 5, 6 ], [ 7, 8 ] ] ]);
assert(
  reshape([ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ], [ 2, 3, 2 ]) ==
  [ [ [ 1, 2 ], [ 3, 4 ], [ 5, 6 ] ], [ [ 7, 8 ], [ 9, 10 ], [ 11, 12 ] ] ]);
assert(reshape([ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ], [12]) ==
       [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ]);
// less dimensions
assert(
  reshape(
    [ [ [ 1, 2 ], [ 3, 4 ], [ 5, 6 ] ], [ [ 7, 8 ], [ 9, 10 ], [ 11, 12 ] ] ],
    [ 4, 3 ]) == [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ], [ 10, 11, 12 ] ]);

assert(
  flatten([ 1, 2, 3, [ 4, 5, [ 6, 7, [ 8, 9 ] ], 10 ], [ 11, 12 ], 13, 14 ]) ==
  [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ]);

// test
assert(multiply(
         [
           1,
           [ 2, 3 ],
           [ 4, 5, 6 ],
         ],
         [ 2, [ 3, 4 ], [ 5, 6, 7 ] ]) == [ 2, [ 6, 12 ], [ 20, 30, 42 ] ]);
